package com.example.moduleapi.controller;

import com.example.moduleapi.service.ModuleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class ModulesController {

    private final ModuleService moduleService;

    public ModulesController(ModuleService ms) {
        this.moduleService = ms;
    }


    @GetMapping("/modules")
    public String modules(){
        return moduleService.getModules();
    }
}



